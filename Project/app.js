
const express = require("express");
const ejs = require("ejs");
const path = require("path");
var bodyParser = require("body-parser");
var session = require('express-session');
var expressValidator = require("express-validator");
var fileUpload = require('express-fileupload');
var passport = require('passport');

var app = express();
//setting up view engine
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

//set static folder
app.use(express.static(path.join(__dirname, "public")));

// //   // Set global errors variable
// app.locals.errors = null;


// Express fileUpload middleware
app.use(fileUpload());
//body parser
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

// Express Session middleware
app.use(session({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true
//  cookie: { secure: true }
}));
// Express Validator middleware
app.use(expressValidator({
  errorFormatter: function (param, msg, value) {
      var namespace = param.split('.')
              , root = namespace.shift()
              , formParam = root;

      while (namespace.length) {
          formParam += '[' + namespace.shift() + ']';
      }
      return {
          param: formParam,
          msg: msg,
          value: value
      };
  },
  customValidators: {
      isImage: function (value, filename) {
          var extension = (path.extname(filename)).toLowerCase();
          switch (extension) {
              case '.jpg':
                  return '.jpg';
              case '.jpeg':
                  return '.jpeg';
              case '.png':
                  return '.png';
              default:
                  return false;
          }
      }
  }
}));

//flash-connect
app.use(require('connect-flash')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  res.locals.message = require('express-messages')(req, res);
  next();
});

//passport config
require('./middleware/passport')(passport);
//passport middleware

app.use(passport.initialize());
app.use(passport.session());

//all routes
app.get('*', function (req, res, next) {
    res.locals.user = req.user || null;
    res.locals.cart=req.session.cart;
    next();
  })
app.post('*', function (req, res, next) {
  res.locals.user = req.user || null;
  res.locals.cart=req.session.cart;
  next();
})
//routing
const root = require("./routes/root");
const productRoute = require("./routes/productRoute");
const cartRoute=require('./routes/cartRoute');
const categoryRoute = require("./routes/categoryRoute");
const userRoute = require("./routes/userRoute");
const loginRoute = require("./routes/loginRoute");

app.use("/", root);
app.use("/product", productRoute);
app.use("/cart",cartRoute);
app.use("/category", categoryRoute);
app.use("/user", userRoute);
app.use("/login", loginRoute);


module.exports = app;
