var productSQL = require('./../helpers/productSQL');
var pSQL = new productSQL();
var fs = require('fs-extra');
var categorySQL = require('./../helpers/categorySQL');
var cSQL = new categorySQL();
const mkdirp = require('mkdirp');
exports.getAddProduct = async (req, res) => {
    console.log(req.body);

    let result = await pSQL.getCategorySQL();
    res.render('product/addProduct', { result });
}

exports.showProducts = async (req, res) => {
    let result = await pSQL.showProductSQL();
    console.log(result);
    res.render('product/allProducts', { result });
}


exports.createProduct = async (req, res) => {
    try {
        var errors=req.validationErrors();
        errors=[];
        const categoryId = req.body.category;
        const product = req.body.product;
        const desc = req.body.description;
        const image = req.files.image.name;
        const price = req.body.price;
        //const image=req.files.image;
        const data = {
            categoryId, product, desc, price, image
        }
        let productName=await pSQL.getProductbyName(product);
        if(productName!=''){
            errors.push({msg:"product with this name already exists"});
            let result=await cSQL.getAllCategories();
            res.render('product/addProduct',{result,messages:errors})
        }
        else{
            let result = await pSQL.createProducts(data);
            console.log(result);
            console.log(req.files.image);
            const imageFile = req.files.image;
            const path = 'public/product-images/' + result.insertId;
            mkdirp(path, (error) => {
                console.log(error);
    
            });
            imageFile.mv(path + '/' + image, (error) => {
                console.log(error);
            })
                req.flash('success',"successfull");
            res.redirect('/product/allProducts');
        }
     


    } catch (error) {
        console.log(error);

    }
}

exports.editProducts = async (req, res) => {
    const id = req.params.id;
    let product = await pSQL.getProductById(id);
    console.log(product);

    res.render('product/editProducts', { product });
}

exports.updateProducts = async (req, res) => {
    const id = req.params.id;
    var newImage = (req.files != null) ? req.files.image.name : '';
    let currentResult = await pSQL.getProductById(id);
    if (currentResult !== "") {
        var currentImage = currentResult.image;
    }
    if(newImage == ""){
        newImage = currentImage;
    }
    var data = req.body;
    let result = await pSQL.updateProductSQL(id, data, newImage);
    const path = 'public/product-images/' + id;
    //checking if we are uploading new image
    //checking if there is already an image 

    
    console.log(path);
    if (req.files != null) {
        fs.remove(path + '/' + currentImage, (err) => {
            console.log(err);
        });
        imageFile=req.files.image;
        imageFile.mv(path + '/' + newImage, (error) => {
            console.log(error);
        })
    }
    res.redirect('/product/allProducts');
}

exports.deleteProducts = async (req, res) => {
    const id = req.params.id;
    console.log(id);
    let result = await pSQL.deleteProductSQL(id);
    res.redirect('/product/allProducts');
}

exports.specificProduct = async (req, res) => {
    try {
        const name = req.params.name;
        var resultCategory = await cSQL.getAllCategories();
        const result = await pSQL.getProductbyName(name);
        res.render('home/product', { result, resultCategory });
    } catch (error) {
        console.log(error);

    }
}
exports.showCategoryProductAdmin = async (req, res) => {
    try {
        const name = req.params.name;
        var resultCategory = await cSQL.getAllCategories();
        const result = await pSQL.getProductbyName(name);
        res.render('admin/category-product-admin', { result, resultCategory });
    } catch (error) {
        console.log(error);

    }
}
