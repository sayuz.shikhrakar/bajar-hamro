    var productSQL=require('./../helpers/productSQL');
    var pSQL=new productSQL();
    var CategorySQL=require('./../helpers/categorySQL');
    var cSQL=new CategorySQL();
    exports.addToCart=async(req,res)=>{
        try {
            console.log("----------------");
                
            console.log(req.params.id);
           
            
            var productId=req.params.id;
            let result=await pSQL.getProductById(productId);
            var name=result.name;
            console.log(result);
            
            var price=result.price;
            if(typeof req.session.cart== "undefined"){
                req.session.cart=[];
                req.session.cart.push({
                    name:name,
                    qty:1,
                    price:price,
                    image:'/product-images/'+result.id+'/'+result.image
                });
            }else{
                var cart=req.session.cart;
                console.log("-===========")                
                console.log(cart);               
                var newItem=true;
                for(var i=0;i<cart.length;i++){
                    if(cart[i].name==name){
                        console.log(name);                        
                        cart[i].qty++;
                        newItem=false;
                        break;
                    }
                }
                    console.log("this is new item:"+newItem);
                    
                if(newItem){
                    cart.push({
                        name:name,
                        qty:1,
                        price:price,
                        image:'/product-images/'+result.id+'/'+result.image
                    })
            
                    
                }
                console.log(cart);
                

            }
            
            
            res.redirect('back');
            
        } catch (error) {
            
        }
        


    }
    exports.checkout=async(req,res)=>{
        if(req.session.cart && req.session.cart.length == 0){
            delete req.session.cart;
            res.redirect('back');
        }
        else{
            var resultCategory=await cSQL.getAllCategories();
            res.render('users/checkout',{
                title:'Checkout',
                cart:req.session.cart,
                resultCategory:resultCategory
    
            });
        }
       

    }

    exports.updateCart=async(req,res)=>{
        try {
            var name=req.params.name;
            var cart=req.session.cart;
            var action=req.query.action;
        
            for (let i = 0; i < cart.length; i++) {
                if(cart[i].name==name){
                    switch(action){
                        case"add":
                        cart[i].qty++; 
                        break;
                        case"remove":
                        cart[i].qty--; 
                        if(cart[i].qty <1) cart.splice(i,1);
                        break;
                        case"clear":
                        cart.splice(i,1);
                        if(cart.length == 0) delete req.session.cart;
                        break;
                        default:
                            console.log('updated problem');
                            break;
                            
                    }
                    break;
                }
                
            }
            res.redirect('back');
        } catch (error) {
            console.log(error);
            
        }
            
        }
    
        exports.clearCart=async(req,res)=>{
            delete req.session.cart;
            res.redirect('back');
           
    
        }