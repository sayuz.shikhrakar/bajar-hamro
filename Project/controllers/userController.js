const userSQL = require('./../helpers/userSQL');
const uSQL = new userSQL();
const categorySQL=require('./../helpers/categorySQL')
const cSQL=new categorySQL();
var bcrypt = require('bcryptjs');
//creating user
exports.createUser = async (req, res) => {
  const data = req.body;

  req.checkBody('firstName', 'field is required!').notEmpty();
  req.checkBody('lastName', 'field is required!').notEmpty();
  req.checkBody('userName', 'field is required!').notEmpty();
  req.checkBody('emailAddress', 'Email is required!').isEmail();
  req.checkBody('gender', 'field is required!').notEmpty();
  req.checkBody('rolePicker', 'role is required!').notEmpty();
  req.checkBody('password', 'Password is required!').notEmpty();
  req.checkBody('confirmPassword', 'Passwords do not match!').equals(data.password);
  var errors = req.validationErrors();

  if (errors.length > 0) {
    console.log(errors);

    res.render('users/register', {
      messages: errors,
      title: 'Register',
      data: data
    });
  } else {
    let error = [];
    let result = await uSQL.getuserByuserName(data.userName);
    let userE = result.User_Name;
    if (result != "") {
      console.log("user exists");

      //req.flash('danger','User Name already exists!');
      error.push({ msg: "user already exists" })
      res.render('users/register', {
        messages: error,
        title: 'Register',
        data: data

      });
    } else {
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(data.password, salt, (err, hash) => {
          if (error)
            console.log(error);
          data.password = hash;

        })
      })
      let createResult = await uSQL.createUserSQL(data);
      req.flash('success', 'you are now registered!');
      res.redirect('/login/login')
    }
  }



}
// show all users
exports.allUsers = async (req, res) => {
  let result = await uSQL.allUserSQL();
  // console.log(result);
  res.render('users/allUsers', { result });
}

//editing users



//update
exports.updateUser = async (req, res) => {
  const id = req.params.id;
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const userName = req.body.userName;
  const emailAddress = req.body.emailAddress;
  const data = {
    id, firstName, lastName, userName, emailAddress
  }
  let result = await uSQL.updateUserSQL(data);
  res.redirect('/user/allUsers');

}


//delete users
exports.deleteUsers = async (req, res) => {
  const id = req.params.id;
  let result = await uSQL.deleteUserSQL(id);
  res.redirect('/user/allUsers');

}

exports.userLogin = (req, res) => {
  const username = req.body.userName;
  const password = req.body.Password;
  var sql = `SELECT FROM users WHERE User_Name='${username}'`;
  con.query(sql, (err, result) => {
    if (err) throw err;
    if (result != '') {
      res.redirect('/');
    } else
      res.send("username/password doesn't match");
  })
}

exports.editUsers = async (req, res) => {
  let id = req.params.id;
  let user = await uSQL.editUserSQL(id);
  res.render('users/editUser', { user });
}
//edit change password
exports.editchangePassword=async(req,res)=>{
  try {
    const id=req.params.id;
    let user = await uSQL.editUserSQL(id);
      res.render('users/changePassword',{user});
  } catch (error) {
    console.log(error);
    
  }
}
//change password
exports.changePassword=async(req,res)=>{
  try {
    const id=req.params.id;
    const data=req.body;
    var errors=req.validationErrors();
    errors=[];
    let user=await uSQL.getuserByuserId(id);
    console.log(user.Password);
    
    
    bcrypt.compare(data.prePassword,user.Password, function (err, isMatch) {   
      if (err)
          console.log(err);

      if (isMatch) {
        if(data.password!=data.confirmPassword){
          errors.push({msg:'passwords does not match'});
          let user= uSQL.getuserByuserId(id);
          res.render('users/changePassword',{user,messages:errors})
        }else{
          bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(data.prePassword, salt, (err, hash) => {
              if (err)
                console.log(err);
              data.password = hash;
    
            })
          })
          let updatepassword=uSQL.updatePassword(data);
          console.log('updated');
          let user =  uSQL.editUserSQL(id);
          res.render('users/editUser',{user});
        }
      } else {
        errors.push({msg:"your previous password is incorrect"});
        console.log("your previous password is incorrect");
        
        let user= uSQL.getuserByuserId(id);
        res.render('users/changePassword',{user,messages:errors})
      }
  });
  } catch (error) {
    console.log(error);
  }
}

