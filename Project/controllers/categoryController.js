
const categorySQl = require('./../helpers/categorySQL');
const cSQL = new categorySQl();
const productSQL=require('./../helpers/productSQL');
const pSQL=new productSQL();
var flash = require('connect-flash');
//create category
exports.createCategory = async (req, res) => {
  console.log("-------------");
  
  console.log(res.locals.user);
  
  var errors = req.validationErrors();
  const name = req.body.name;
  let nameResult = await cSQL.getcategoryByName(name);
  console.log(nameResult);

  errors = [];

  if (nameResult.length != 0) {
    errors.push({ msg: 'this category already exists' });
    req.session.errors = errors;
    res.render('category/addCategory', { messages:errors })
  }
  else {
    const name = req.body.name;
    const desc = req.body.description;

    var data = {
      name, desc
    }
    let result = await cSQL.createCategories(data);
    console.log('from createCategory function of controller');
    console.log(result);
    // req.flash('msg', 'Category Update')
    res.redirect('/category/showCategories');
  }


}

//show categories
exports.showCategories = async (req, res) => {
  let category = await cSQL.getAllCategories();
  console.log('from Controller');
  //console.log(category);     
  res.render('category/allCategories', { category });
}


//get edit categories
exports.editCategory = async (req, res) => {
  const id = req.params.id;
  let category = await cSQL.getcategoryById(id);
  res.render('category/editCategory', {category});
}

//update categories
exports.postUpdateCategory = async (req, res) => {
    const id = req.params.id;
    const name = req.body.name;
    const description = req.body.description;
    var errors=[];
    const data = {
      id, name, description
    }
    let categoryBYName=await cSQL.getcategoryByName(name);
    if(categoryBYName!=""){
      let category = await cSQL.getcategoryById(id);
      if(name==category.name){
         let updateCategory = await cSQL.updateCategory(data);
        let category=await cSQL.getAllCategories();
        res.render('category/allCategories',{category})
      }else{
              errors.push({msg:"category already exists"});
        res.render('category/editCategory', {category,messages:errors});
      }
    }
  else{
        let updateCategory = await cSQL.updateCategory(data);
        let category=await cSQL.getAllCategories();
        res.render('category/allCategories',{category})
      }
}

//delete categories
exports.deleteCategories = async (req, res) => {
  const id = req.params.id;
  let categoryStatus = await cSQL.deleteCategorySQL(id);
  console.log(categoryStatus);
  let category = await cSQL.getAllCategories();
  res.render('category/allCategories', { category });

}

exports.getSpecificCategory=async(req,res)=>{
  try {
    const id=req.params.id;
    console.log(id);
    var resultProduct=await pSQL.getallProductsByCategory(id);
    console.log(resultProduct);
    
    var resultCategory=await cSQL.getAllCategories();
    //var resultProduct= await pSQL.showProductSQL();  
 
    res.render('product/category-products', { resultCategory,resultProduct});
   
  } catch (error) {
    console.log(error);
    
  }
 
}

