const LocalStrategy = require('passport-local').Strategy;
const loginSQL = require('./../helpers/loginSQL');
const bcrypt = require('bcryptjs');
const lSQL = new loginSQL();

module.exports = (passport) => {
  passport.use(
    new LocalStrategy(
      async (username, password, done) => {
        try {
          let user = await lSQL.searchbyUserName(username);
          
          if (user.length == 0) {
            return done(null, false, { messages: 'the account with this username does not exists' });}
            bcrypt.compare(password, user.Password, function (err, isMatch) {
              if (err)
             {
              console.log(err);
             }       
              if (isMatch) {
            
                  return done(null, user);
              } else {   
                  return done(null, false, {messages: 'Wrong password.'});
                  
              }
          });
        } catch (err) {
          console.log(err);
        }
      }
    )
  )
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  passport.deserializeUser(async (id, done) => {
    try {
      let user = await lSQL.searchbyId(id);
        await done(null, user);
    } catch (error) {
      throw error;
    }
  });
};
