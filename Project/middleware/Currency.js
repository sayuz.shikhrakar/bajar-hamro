const Fixer = require('fixer-node')
const fixer = new Fixer('access-key',{https:true})
module.exports=class Currency{
    async Converter(){
        try {
            const data = await fixer.symbols()  
        const latest = await fixer.latest({ symbols: 'EUR, USD', base: 'NPR' })
        const convert = await fixer.convert({ from: 'NPR', to: 'EUR', amount: 25 })
        return convert;
        } catch (error) {
            console.log(error);
            
        }
        
    }
    
}


