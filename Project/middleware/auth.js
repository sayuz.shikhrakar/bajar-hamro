exports.isUser=(req,res,next)=>{
    if(req.isAuthenticated() && res.locals.user.Roles=='User'){
        console.log("login as user");        
        next();
        
    }else{
        req.flash('danger','please log in');
        res.render('login/login');
    }
}

exports.isAdmin=(req,res,next)=>{       
    if(req.isAuthenticated()&& res.locals.user.Roles=='Admin' ){
        
        console.log("login as admin");
        next();
    }else{
         console.log("unsuccessfull");
        req.flash('danger','please log as admin');
        res.render('login/login');
    }
}
