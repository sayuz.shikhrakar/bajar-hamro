var express = require('express');
const router = express.Router();
const categoryController = require('./../controllers/categoryController');
const productController = require('../controllers/productController');
const accountController = require('./../controllers/accountController');
const auth=require('./../middleware/auth');


//route to add product
router.get('/addProduct',auth.isAdmin,productController.getAddProduct);

router.post('/addProduct',productController.createProduct);
//show products     
router.get('/allProducts',auth.isAdmin, productController.showProducts);

router.get('/category-product-admin/:name',auth.isAdmin,productController.showCategoryProductAdmin);

router.get('/edit/:id',productController.editProducts);

router.post('/edit/:id',productController.updateProducts);
//delete the products
router.get('/delete/:id',productController.deleteProducts);

//sepcific product
router.get('/show/:name',productController.specificProduct);
  

module.exports = router;