var express = require('express');
var productSQL = require('./../helpers/productSQL');
var auth=require('./../middleware/auth');
var pSQL = new productSQL();

var categorySQL=require('./../helpers/categorySQL');
var cSQL=new categorySQL();
const router = express.Router();


//get index page
router.get('/',async(req,res)=>{
  try {
     //get product
  var resultProduct= await pSQL.showProductSQL();   
  var resultCategory=await cSQL.getAllCategories();
  res.render('home/home', { resultProduct,resultCategory });
  } catch (error) {
    console.log(error);
    
  }
 

});

module.exports = router;