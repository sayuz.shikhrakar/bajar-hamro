var express = require('express');
var router = express.Router();
var passport = require('passport');
var bcrypt = require('bcryptjs');
var flash = require('connect-flash');

//login
router.get('/login', (req, res) => {
    if (res.locals.user) res.redirect('/');
    res.render('login/login', {
        title: 'Log in'
    });

})

router.post('/login', function (req, res, next) {

    passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login/login',
        failureFlash: true
    })(req, res, next);

});

//logout
router.get('/logout', (req, res) => {
    console.log('logging out');
    req.logout();
    req.flash('success', 'you are logged out');
    res.redirect('/login/login');
})

module.exports = router;