var express=require('express');
const router=express.Router();
const cartController=require('./../controllers/cartController')
router.get('/add/:id',cartController.addToCart);
//checkout page
router.get('/users/checkout',cartController.checkout);

//for updating
router.get("/update/:name",cartController.updateCart);

//clear cart page
router.get('/clear',cartController.clearCart);

module.exports=router;