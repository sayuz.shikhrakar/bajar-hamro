var express = require('express');
var router = express.Router();
var userController = require('./../controllers/userController');


//create users
router.get('/createUser', function (req, res, next) {
  res.render('users/register',{title:'from validation', success:req.session.success,errors:req.session.errors});
  req.session.errors=null;
});

router.post('/createUser',userController.createUser);


//show all users
router.get('/allUsers', userController.allUsers);
//edit users
router.get('/edit/:id', userController.editUsers);

router.post('/edit/:id', userController.updateUser);

//delete users from table
router.get('/delete/:id',userController.deleteUsers);

//get change password
router.get('/changePassword/:id',userController.editchangePassword);
//post change password
router.post('/changePassword/:id',userController.changePassword);


//user login
router.get('/userLogin',(req,res)=>{
  res.render('users/userLogin')
});
router.post('/userLogin',userController.userLogin);
module.exports = router;
