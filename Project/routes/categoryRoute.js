var express = require('express');
const router = express.Router();
const categoryController = require('./../controllers/categoryController');
const productController = require('../controllers/productController');
const auth=require('./../middleware/auth')

//route to add category
router.get('/addCategory',auth.isAdmin,(req, res) => {
    res.render('category/addCategory', {});
})
router.post('/addCategory',auth.isAdmin,categoryController.createCategory);

  //route to show categories
router.get('/showCategories',auth.isAdmin, categoryController.showCategories);

//route to edit categories
router.get('/edit/:id', categoryController.editCategory);

router.post('/edit/:id', categoryController.postUpdateCategory);

//route to delete categories

router.get('/delete/:id',categoryController.deleteCategories);

//show category products
router.get('/category-show/:id',categoryController.getSpecificCategory);

module.exports = router;