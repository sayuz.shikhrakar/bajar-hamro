const gcon=require('./../middleware/connection');


module.exports=class productSQL{

    async getCategorySQL(){
        try {
            let con=await gcon();
            const readSQL=`SELECT * FROM category`;
            let status=await con.query(readSQL);
             status=JSON.parse(JSON.stringify(status));
            return status;
        } catch (error) {
            console.log(error);            
        }       
    }

    //async/await for creating products
    async createProducts(data){
        try {
                let con=await gcon();
                let readSQL = `INSERT INTO product(category_id,name,price,image,description) VALUE('${data.categoryId}','${data.product}','${data.price}','${data.image}','${data.desc}')`;
                let status = await con.query(readSQL);
                return status;
            
        } catch (error) {
            console.log(error);
        }

    }

    async showProductSQL(){
        try {
            let con=await gcon();
            const readSQL=`SELECT * FROM product ORDER BY name`;
            let productStatus=await con.query(readSQL);
            productStatus=JSON.parse(JSON.stringify(productStatus));

            return productStatus;

            
        } catch (error) {
            console.log(error);
            
        }

    }

    async getProductById(id){
        try {
            let con=await gcon();
            const readSQL=`SELECT * FROM product WHERE id='${id}'`;
            let status=await con.query(readSQL);
            status=JSON.parse(JSON.stringify(status));
            status.forEach(element=>{
                status=element;
            })
            return status;

        } catch (error) {
            console.log(error);
            
        }
    }

    async updateProductSQL(id,data,image){
        try {
            let con =await gcon();
            const readSQL=`UPDATE product SET name = '${data.name}', price='${data.price}',description='${data.description}',image='${image}' WHERE id = '${id}'`;
            let status=await con.query(readSQL);
             status=await JSON.parse(JSON.stringify(status));
            return status;
        } catch (error) {
            console.log(error);
            
        }
    }

    async deleteProductSQL(id){
        try {
            let con=await gcon();
            const readSql=`DELETE FROM product WHERE id='${id}'`;
            let result=await con.query(readSql);
            result = JSON.parse(JSON.stringify(result));
            return result;
            
        } catch (error) {
            console.log(error);
            
        }
    }

    async getProductbyName(name){
try {
    
    let con=await gcon();
    const readSQL=`SELECT * FROM product WHERE name='${name}'`;
    var result=await con.query(readSQL);
    result = JSON.parse(JSON.stringify(result)); 
    return result;  
} catch (error) {
    console.log(error);
    
}
    }
    async getallProductsByCategory(id){
        try {
            let con=await gcon();
    const readSQL=`SELECT * FROM product WHERE category_id='${id}'`;
    var result=await con.query(readSQL);
    result = JSON.parse(JSON.stringify(result));

    return result;  
        } catch (error) {
            console.log(error);
            
        }
    }
}
