const gcon = require('./../middleware/connection');
const express = require('express')
const router = express.Router();



module.exports = class categorySQL {
    //async/await for showing categories
    async getAllCategories() {
        try {
            let con = await gcon();
            const readSQL = `SELECT * FROM category`;
            let status = await con.query(readSQL);
            status = JSON.parse(JSON.stringify(status));
            return status;
        } catch (error) {
            console.log(error);
        }
    }

    //async/await for creating categories
    async createCategories(data) {
        console.log(data);
        try {
            let con = await gcon();

            const readSQL = `SELECT * FROM category WHERE name='${data.name}'`;
            let status = await con.query(readSQL);
            status = JSON.parse(JSON.stringify(status));
            if (status != '') {
                console.log('Category already exists');

            } else {
                let readSQL = `INSERT INTO category (name,description) VALUE ('${data.name}','${data.desc}')`;
                let status = await con.query(readSQL);
                return status;
            }
        } catch (error) {
            console.log(error);
        }
    }


    //async/await for editing categories
    async editCategory(id) {

        try {
            let con = await gcon();
            const readSQL = `SELECT * FROM category WHERE id=${id}`;
            let status = await con.query(readSQL);
            status = JSON.parse(JSON.stringify(status));
            status.forEach(category1 => {
                status = category1;
            });
            return status;
        } catch (error) {
            console.log(error);

        }



    }

    //async/await for updating categories
    async updateCategory(data) {
        try {
            let con = await gcon();
            var sql = `UPDATE category SET name = '${data.name}',description='${data.description}' WHERE id = '${data.id}'`;
            let status = await con.query(sql);
            status= await JSON.parse(JSON.stringify(status));
            return status;
        }
        catch (error) {
            console.log(error);

        }

    }


    //async/await for deleting categories
    async deleteCategorySQL(id) {
        try {
            let con = await gcon();
            var readSql = `DELETE FROM category WHERE id='${id}'`;
            let status = await con.query(readSql);
            return status;

        } catch (err) {
            console.log(err);

        }
    }

    async getcategoryByName(name) {
        try {
            let con = await gcon();
            const readSQL = `SELECT * FROM category WHERE name='${name}'`;
            let status = await con.query(readSQL);
            status = JSON.parse(JSON.stringify(status));
            return status;

        } catch (error) {
            console.log(error);

        }
    }

    async getcategoryById(id) {
        try {
            let con = await gcon();
            const readSQL = `SELECT * FROM category WHERE id='${id}'`;
            let status = await con.query(readSQL);
            status = JSON.parse(JSON.stringify(status));
            status.forEach(element=>{
                status=element;
            })
            return status;

        } catch (error) {
            console.log(error);
        }
    }

}
