const gcon = require('./../middleware/connection');

module.exports = class userSQL {
    //async/await to creata user
    async createUserSQL(data) {
        try {
            let con = await gcon();
                const readSQL = `INSERT INTO users (First_Name,Last_Name,User_Name,Email_Address,Password,Gender,Roles) VALUE ('${data.firstName}','${data.lastName}','${data.userName}','${data.emailAddress}','${data.password}','${data.gender}','${data.rolePicker}')`;
                let status = await con.query(readSQL);
                return status;
            }
         catch (error) {
            console.log(error);

        }
    }

    //async/await to get all users
    async allUserSQL() {
        try {
            let con = await gcon();
            const readSQL = `SELECT * FROM users`;
            let status = await con.query(readSQL);
            return status;

        } catch (error) {
            console.log(error);

        }
    }

    //async/await to edit users
    async editUserSQL(id) {
        try {
            let con = await gcon();
            const readSQL = `SELECT * FROM users WHERE id='${id}'`;
            let status = await con.query(readSQL);
            status = JSON.parse(JSON.stringify(status));
            status.forEach(element => {
                status = element;
            })
            return status;
        } catch (error) {
            console.log(error);

        }
    }

    //async/await to update users
    async updateUserSQL(data) {
        try {
            let con = await gcon();
            const readSQL = `UPDATE users SET First_Name='${data.firstName}',Last_Name='${data.lastName}',User_Name='${data.userName}',Email_Address='${data.emailAddress}' WHERE id='${data.id}' `;
            let status = await con.query(readSQL);
            return status;
        } catch (error) {
            console.log(error);

        }
    }

    //async/await delete users
    async deleteUserSQL(id) {
        try {
            let con = await gcon();
            const readSQL = `DELETE FROM users WHERE id='${id}'`;
            let status = await con.query(readSQL);
            return status;
        } catch (error) {

        }
    }
    
    async getuserByuserName(userName){
        try {
            let con = await gcon();
            const readSQL = `SELECT * FROM users WHERE User_Name='${userName}'`;
            let status = await con.query(readSQL);
             status=await JSON.parse(JSON.stringify(status));
            return status;

        } catch (error) {
            console.log(error);

        }
    }

    async getuserByuserId(id){
        try {
            let con = await gcon();
            const readSQL = `SELECT * FROM users WHERE id='${id}'`;
            let status = await con.query(readSQL);
             status=await JSON.parse(JSON.stringify(status));
              status=status.reduce(ele=>{
                 return ele;
             })
            return status;

        } catch (error) {
            console.log(error);

        }
    }
    async getuserByEmail(emailAddress){
        try {
            let con = await gcon();
            const readSQL = `SELECT * FROM users WHERE Email_Address='${emailAddress}'`;
            let status = await con.query(readSQL);
            return status;

        } catch (error) {
            console.log(error);

        }
    }

      //async/await to update oassword
      async updatePassword(data,id) {
        try {
            let con = await gcon();
            const readSQL = `UPDATE users SET Password='${data.password}' WHERE id='${id}' `;
            let status = await con.query(readSQL);
            return status;
        } catch (error) {
            console.log(error);

        }
    }


}